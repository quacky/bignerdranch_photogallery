package com.android.bignerdranch.photogallery.api

/**
 * holds the outermost object in the received json object by flickr
 */
class FlickrResponse {
    lateinit var photos : PhotoResponse
}
package com.android.bignerdranch.photogallery.api

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

/**
 * retrofit API Interface:
 * each function in the interface maps to a specific HTTP request.
 */
interface FlickrApi {
    @GET("/")
    fun fetchContents(): Call<String>

    @GET("services/rest/?method=flickr.interestingness.getList")
    fun fetchPhotos(): Call<FlickrResponse>

    @GET
    fun fetchURLBytes(@Url url: String): Call<ResponseBody>

    @GET("services/rest/?method=flickr.photos.search")
    fun searchPhoto(@Query("text") queryString: String): Call<FlickrResponse>

}
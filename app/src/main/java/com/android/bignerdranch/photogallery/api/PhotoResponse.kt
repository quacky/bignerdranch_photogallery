package com.android.bignerdranch.photogallery.api

import com.android.bignerdranch.photogallery.GalleryItem
import com.google.gson.annotations.SerializedName

/**
 * Contains the information of the rest api response mainly the list
 * with photos (galleryItems)
 */
class PhotoResponse{
    @SerializedName("photo")
    lateinit  var galleryItems: List<GalleryItem>
}
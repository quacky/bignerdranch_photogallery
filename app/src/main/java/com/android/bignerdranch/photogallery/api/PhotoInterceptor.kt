package com.android.bignerdranch.photogallery.api

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

/**
 * An interceptor does what you might expect – it intercepts a request or response and allows you to
 * manipulate the contents or take some action before the request or response completes.
 *
 */

private const val API_KEY = "eb6d547f285f51fbe049cc72e4d2e60c"

class PhotoInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest: Request = chain.request()

        val newUrl: HttpUrl =
            originalRequest.url().newBuilder().addQueryParameter("api_key", API_KEY)
                .addQueryParameter("extras", "url_s")
                .addQueryParameter("format", "json")
                .addQueryParameter("nojsoncallback", "1")
                .addQueryParameter("safesearch", "1")
                .build()

        val newRequest: Request = originalRequest.newBuilder().url(newUrl).build()
        return chain.proceed(newRequest)
    }
}
package com.android.bignerdranch.photogallery

import android.os.Bundle
import android.os.StrictMode
import androidx.appcompat.app.AppCompatActivity

class PhotoGalleryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_gallery)
        StrictMode.enableDefaults()

        // check if fragment is already hosted
        // if savedInstanceState is null this is a fresh start of the activity
        // and nothing was restored.
        val isFragmentContainerEmpty = savedInstanceState == null
        if (isFragmentContainerEmpty) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, PhotoGalleryFragment.newInstance())
                .commit()
        }
    }
}
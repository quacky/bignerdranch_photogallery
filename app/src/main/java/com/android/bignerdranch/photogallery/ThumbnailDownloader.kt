package com.android.bignerdranch.photogallery

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Handler
import android.os.HandlerThread
import android.os.Message
import android.util.Log
import android.util.LruCache
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import java.util.concurrent.ConcurrentHashMap

private const val TAG = "ThumbnailDownloader"
private const val MESSAGE_DOWNLOAD = 0

/**
 * Background Thread class that downloads photos
 *
 * @param T PhotoHolder in that case.
 * @property responseHandler that should be informed if the object is downloaded
 * @property onThumbnailDownload function as listener. Will be called if a photo is downloaded.
 * The calling task can then decide what to happen with the bitmap.
 */
class ThumbnailDownloader<in T>(
    private val responseHandler: Handler,
    private val onThumbnailDownload: (T, Bitmap) -> Unit
) : HandlerThread(TAG) {

    private var hasQuit = false
    private lateinit var requestHandler: Handler

    //concurrentHashMap = thread save HashMap:
    private val requestMap = ConcurrentHashMap<T, String>()
    private val flickrFetch = FlickrFetchr()


    private val cacheSize = 64 * 1024 * 1024 // 64MB
    private val bitmapCache: LruCache<String, Bitmap> =
        object : LruCache<String, Bitmap>(cacheSize) {
            // normally, lru size is measured in the number of items
            // overwrite sizeOf to measure image sizes
            override fun sizeOf(key: String, value: Bitmap): Int {
                return value.byteCount;
            }
        }

    override fun quit(): Boolean {
        hasQuit = true
        return super.quit()
    }

    val fragmentLiveCycleObserver: LifecycleObserver = object : LifecycleObserver {
        @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
        fun setup() {
            Log.i(TAG, "Starting background thread")
            start()
            looper
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun tearDown() {
            Log.i(TAG, "Destroying background thread")
            quit()
        }

    }

    val viewLifecycleObserver: LifecycleObserver = object : LifecycleObserver {
        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun tearDown() {
            Log.i(TAG, "Clearing all request from queue")
            requestHandler.removeMessages(MESSAGE_DOWNLOAD)
            requestMap.clear()
        }
    }

    fun cleanUp() {
        requestHandler.removeMessages(MESSAGE_DOWNLOAD)
        requestMap.clear()
    }


    @Suppress("UNCHECKED_CAST")
    @SuppressLint("HandlerLeak")
    override fun onLooperPrepared() {
        requestHandler = object : Handler() {
            /**
             * defines what the Handler will do if a message is pulled of the queue.
             * @param msg the message that is taken
             */
            override fun handleMessage(msg: Message) {
                if (msg.what == MESSAGE_DOWNLOAD) {
                    val target = msg.obj as T
                    Log.i(TAG, "Got a request URL: ${requestMap[target]}")
                    handleRequest(target)
                }
            }
        }
        super.onLooperPrepared()
    }

    /**
     * Helper function where the download happens.
     * Grep url and download photo via flickrFetch
     *
     * @param target
     */
    private fun handleRequest(target: T) {
        val url = requestMap[target] ?: return

        //  val bitmap = bitmapCache.get(url) ?: flickrFetch.fetchPhoto(url) ?: return
        var bitmap = bitmapCache.get(url)
        if (bitmap != null) {
            Log.i(TAG, "Catched url : ${url} out of cache")
        } else {
            bitmap = flickrFetch.fetchPhoto(url) ?: return
            Log.i(TAG, "Freshly download : ${url}")
        }

        responseHandler.post(Runnable {
            // all of that code will be executed in the main thread because
            // post is attached to the responseHandler
            if (requestMap[target] != url || hasQuit) {
                // recycler view may have recycled the photoholder and requested another photo
                // in the meantime. In that case, discard the old one and wait for the new one.
                return@Runnable
            }
            requestMap.remove(target)
            bitmapCache.put(url, bitmap)
            onThumbnailDownload(target, bitmap)
        })
    }

    /**
     * Create a new message and add it to the queue.
     * The new message represents a download object.
     *
     * @param target the message object, in this case a PhotoHolder
     * @param url the url of the target
     */
    fun queueThumbnail(target: T, url: String) {
        Log.i(TAG, "Got a URL: $url")
        // remember the mapping url<->photoholder.
        // this ensures to always download the most recent URL of the photo holder object
        // (remember, ViewHolders in RecyclerViews are recycled ;-) )
        requestMap[target] = url
        requestHandler.obtainMessage(MESSAGE_DOWNLOAD, target).sendToTarget()
    }
}
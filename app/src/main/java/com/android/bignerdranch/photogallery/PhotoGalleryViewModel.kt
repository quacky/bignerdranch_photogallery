package com.android.bignerdranch.photogallery

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations

private const val TAG = "PhotoGallerViewModel"

class PhotoGalleryViewModel(private val app: Application) : AndroidViewModel(app) {

    val galleryItemLiveData: LiveData<List<GalleryItem>>

    private val flickrFetchr = FlickrFetchr()
    private val mutableSearchTerm = MutableLiveData<String>()

    init {
        Log.d(TAG, "ViewModel instance created")
        // live data transformation: every time the trigger is updated, a new live data object
        // is returned (according to the mapping function)
        mutableSearchTerm.value = QueryPreferences.getStoredQuery(app)
        galleryItemLiveData = Transformations.switchMap(mutableSearchTerm) { searchTerm ->
            flickrFetchr.searchPhotos(searchTerm)
        }
    }

    fun fetchPhotos(query: String) {
        QueryPreferences.setStoredQuery(app, query)
        mutableSearchTerm.value = query
    }


    override fun onCleared() {
        super.onCleared()
        Log.d(TAG, "ViewModel instance cleared.")
    }
}
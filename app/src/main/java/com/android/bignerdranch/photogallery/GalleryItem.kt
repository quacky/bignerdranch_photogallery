package com.android.bignerdranch.photogallery

import com.google.gson.annotations.SerializedName

/**
 * A galler item holds meta information for a single photo,
 * including title, ID and URL
 * Additionally, it is a mapping to the json object which will be received.
 */
data class GalleryItem (
    // no mapping to gson needed because property name is identical
    var title: String = "",
    var id: String ="",
    // tell gson how the property is called in the json object:
    @SerializedName("url_s") var url: String = ""
)
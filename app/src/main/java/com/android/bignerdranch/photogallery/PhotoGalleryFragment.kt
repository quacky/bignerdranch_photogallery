package com.android.bignerdranch.photogallery

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.ImageView
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder

private const val TAG = "PhotoGalleryFragment"

class PhotoGalleryFragment : Fragment() {

    private lateinit var photoRecyclerView: RecyclerView
    private lateinit var thumbnailDownloader: ThumbnailDownloader<PhotoHolder>
    private lateinit var searchView: SearchView

    // remember, the viewModel stays in memory if the configuration is destroy (e.g. rotation)
    // lazy: will initialized (when and only) if the thing is needed for the first time
    private val photoGalleryViewModel: PhotoGalleryViewModel by lazy {
        ViewModelProviders.of(this).get(PhotoGalleryViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // not recommended, but simplifies the implementation during configuration changes
        // drawback: handles only rotation but not if the OS changes the configuration change.
        retainInstance = true

        // register option menu -> to receive callbacks
        setHasOptionsMenu(true)

        // define the message handler and the action if a message is received.
        val responseHandler = Handler()
        thumbnailDownloader = ThumbnailDownloader(
            responseHandler
        ) { holder: PhotoHolder, bitmap: Bitmap ->
            Log.d(TAG, "photo received")
            val bitmapDrawable = BitmapDrawable(bitmap)
            holder.bindDrawable(bitmapDrawable)
        }

        lifecycle.addObserver(thumbnailDownloader.fragmentLiveCycleObserver)


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_item_clear -> {
                Log.d(TAG, "Clear Item pressed")
                photoGalleryViewModel.fetchPhotos("")
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_photo_gallery, menu)
        val searchItem = menu.findItem(R.id.menu_item_search)
        searchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                Log.d(TAG, "Query string $query received")
                photoGalleryViewModel.fetchPhotos(query)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                Log.d(TAG, "Query text changed: $newText")
                return false
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        lifecycle.addObserver(thumbnailDownloader.viewLifecycleObserver)

        // inflate Grid Layout:
        val view = inflater.inflate(R.layout.fragment_photo_gallery, container, false)
        photoRecyclerView = view.findViewById(R.id.photo_recycler_view)
        photoRecyclerView.layoutManager = GridLayoutManager(context, 3)


        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // see also chapter "Observing Live data"
        // observe data change in the own view model. -> persistent over configuration change.
        photoGalleryViewModel.galleryItemLiveData.observe(
            viewLifecycleOwner,
            { galleryItems ->
                Log.d(TAG, "Response received: $galleryItems")
                // connect with adapter:
                photoRecyclerView.adapter = PhotoAdapter(galleryItems)
            })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        lifecycle.removeObserver(thumbnailDownloader.viewLifecycleObserver)
    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycle.removeObserver(thumbnailDownloader.fragmentLiveCycleObserver)
    }

    // Wie ein singleton, gibt es nur einmal ansonsten das bereits existierende object.
// TODO: nochmal nachschlagen
    companion object {
        fun newInstance() = PhotoGalleryFragment()
    }

    private class PhotoHolder(private val itemImageView: ImageView) :
        ViewHolder(itemImageView) {

        // anonymous function to set text, unit -> "void" return value of function
        // see Kotlin Programming, chapter "the function type"
        //   val bindTitle:(CharSequence) -> Unit = {itemView.text = it}
        // same as line before but with function reference operator ::
        val bindDrawable: (Drawable) -> Unit = { itemImageView.setImageDrawable(it) }

    }

    private inner class PhotoAdapter(
        val galleryItems: List<GalleryItem>
    ) :
        RecyclerView.Adapter<PhotoHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoHolder {
            val imageView =
                layoutInflater.inflate(R.layout.list_item_gallery, parent, false) as ImageView
            return PhotoHolder(itemImageView = imageView)
        }

        override fun onBindViewHolder(holder: PhotoHolder, position: Int) {
            val placeholder: Drawable =
                ContextCompat.getDrawable(requireContext(), R.drawable.bill_up_close)
                    ?: ColorDrawable()
            thumbnailDownloader.queueThumbnail(holder, galleryItems[position].url)
            holder.bindDrawable(placeholder)
        }

        override fun getItemCount(): Int {
            return galleryItems.size
        }

    }

}

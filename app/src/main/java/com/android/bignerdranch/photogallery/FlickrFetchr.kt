package com.android.bignerdranch.photogallery

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.bignerdranch.photogallery.api.FlickrApi
import com.android.bignerdranch.photogallery.api.FlickrResponse
import com.android.bignerdranch.photogallery.api.PhotoInterceptor
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val TAG = "FlickrFetch"

/**
 * FlickrFetch will handle most of the networking code in PhotoGallery
 */
class FlickrFetchr {
    private val flickrApi: FlickrApi

    init {
        // use an interceptor which always adds the same parameters in url
        // -> use own client instead of default client
        val client = OkHttpClient.Builder().addInterceptor(PhotoInterceptor()).build()
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://www.flickr.com/")
            // scalarsconverter -> convert responsebody to string object (outdated)
            // gscon converter -> convert responsebody to kotlin classes
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
        // create a retrofit instance (at runtime) based on the specified FlickrApi interface
        flickrApi = retrofit.create(FlickrApi::class.java)

    }

    /**
     * Query for the default interesting photos on flickr
     *
     * @param query that should be used.
     * @return query result as live data
     */
    fun fetchPhotos(): LiveData<List<GalleryItem>> {
        return fetchPhotoMetaData(flickrApi.fetchPhotos())
    }

    /**
     * Search for the given query string on flickr
     *
     * @param query
     * @return query result as live data
     */
    fun searchPhotos(query: String): LiveData<List<GalleryItem>> {
        return if (query.isEmpty()) {
            // app crashes if the query string is empty
            fetchPhotos()
        } else {
            fetchPhotoMetaData(flickrApi.searchPhoto(queryString = query))
        }
    }

    /**
     * Executes the given call object asynchron in a queue.
     *
     * @param flickrRequest call object that represents the web request. Remark, the call object
     * is not the answer of the web request. It will be executed in this method.
     * @return response live data of the executed web request.
     */
    private fun fetchPhotoMetaData(flickrRequest: Call<FlickrResponse>): LiveData<List<GalleryItem>> {
        val responseLiveData: MutableLiveData<List<GalleryItem>> = MutableLiveData()

        // enqueue starts the asynchronous web request
        // the callback object defines what happens if it is finished
        flickrRequest.enqueue(object : Callback<FlickrResponse> {
            override fun onFailure(call: Call<FlickrResponse>, t: Throwable) {
                Log.e(TAG, "Failure to fetch photos", t)
            }

            override fun onResponse(
                call: Call<FlickrResponse>,
                response: Response<FlickrResponse>
            ) {
                Log.d(TAG, "Response received: ${response.body()}")
                val flickrResponse = response.body()
                val photoResponse = flickrResponse?.photos
                var galleryItems: List<GalleryItem> = photoResponse?.galleryItems ?: mutableListOf()
                // filter out items which does not have a url:
                galleryItems = galleryItems.filterNot { it.url.isBlank() }
                responseLiveData.value = galleryItems
            }
        })
        return responseLiveData
    }

    @WorkerThread
    // worker thread ensures that this thing is only called on a background thread
    fun fetchPhoto(url: String): Bitmap? {
        val response: Response<ResponseBody> = flickrApi.fetchURLBytes(url).execute()
        val bitmap = response.body()?.byteStream()?.use(BitmapFactory::decodeStream)
        // probably the same:
        // val bitmap = response.body()?.byteStream()?.use { it -> BitmapFactory.decodeStream(it) }
        Log.i(TAG, "Decoded bitmap =$bitmap from Response:$response")
        return bitmap
    }
}